var connect = require('connect');
var serveStatic = require('serve-static');
// Load asciidoctor.js and asciidoctor-reveal.js
var asciidoctor = require('asciidoctor.js')();
require('asciidoctor-reveal.js');

// Convert the document 'index.adoc' using the reveal.js converter
var attributes = {'revealjsdir': 'node_modules/reveal.js@'};
var options = {safe: 'safe', backend: 'revealjs', attributes: attributes};
asciidoctor.convertFile('index.adoc', options); 
connect().use(serveStatic('.')).listen(8090, function(){
    console.log('Server running on 8090 (http://127.0.0.1:8090)...');
});
