// Theme
:revealjs_customtheme: css/theme/bttf.css

:revealjs_slideNumber: true
//:revealjsdir: https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0

:revealjs_width: "100%"
:revealjs_height: "100%"
:revealjs_margin: 0
:revealjs_minScale: 1
:revealjs_maxScale: 1
:revealjs_fragmentInURL: true
:revealjs_history: true
:revealjs_previewLinks: true
:revealjs_plugin_pdf: enabled

:revealjs_transition: fade

:source-highlighter: highlightjs
:highlightjs-theme: ./css/highlight-styles/vs-bttf.css

:navigation:
:menu:
:status:
:imagesdir: images
:title-slide-transition: zoom
:title-slide-transition-speed: fast


= Sfeir Lunch < : [title-left-part]#to the# futur

== STARRING

image::adrien.png[size=contain, role="actor"]

Adrien Pessu as [.actor-role]#Marty#

Freelance

🐦 @adrienpessu


== INTRODUCTION

image::kotlin-text.svg[size=contain]

[NOTE.speaker]
--
* JetBrain
* Iles
* 2011
* Apache2
* Google -> Mai 2017
* github 2.6 x plus de dev en 1 an
* 40% des 1000 apps les plus dl sur le playstore
* kotlinConf 2 fois + d'utilisateur tous les mois (Andrey Breslav )
* Null-safety
* pas concise mais facile à lire
* pas expressif mais reutilisable
* Pas orginial mais interroperable
* sûr et fournit avec des outils
* Pas exhaustif 
--

== INTRODUCTION

image::PYPL.jpg[size=contain]
[NOTE.speaker]
--
L'indice PYPL évalue la popularité des langages en fonction de la fréquence de recherche des didacticiels sur Google.
--

== LES BASES DE LA SYNTAXE

=== VAL VS VAR

// Adrien

[source,kotlin,subs="quotes"]
----
val name = "McFly"

var quote: String = ""

quote = """ Allô ? Allô ?
| Y'a personne au bout du fil ?
| Faut réfléchir ${name} !
| Faut réfléchir
"""

var talk: String? = null
----

[NOTE.speaker]
--
val correspond à une valeur non modifiable.
Doit être initialisée lors de la déclaration.

var peut varier.
Peut être initialisé plus tard.
--

=== TYPE BASIQUE



[%step]
* Pas de type primitif
* Any
* Double, Float, Int, Char, String, ...
* API Collection



=== RANGE

// Adrien

[source,kotlin]
----
if (i in 1..3) {
  println("Retour vers le futur ${i}")
}

for (i in IntRange(1, 3)) {
  println("Retour vers le futur ${i}")
}
----


=== RANGE

// Adrien

[source,kotlin]
----
for (i in 1..4 step 2) {
  println("Retour vers le futur ${i}")
}

for (i in 4 downTo 1 step 2) {
  println("Retour vers le futur ${i}")
}
----

=== SMART CAST

[source,kotlin]
----
if (talk is String)
  print(talk.length)
----


=== SMART CAST

[source,kotlin]
----
val talk: String? = kotlin as String
val conf: String? = SfeirLunch as String?
val speaker: String? = mcFly as? String
----

[NOTE.speaker]
--
unsafe throw exception / safe return null
--

=== DÉCLARATION FONCTIONS

[source,kotlin,subs="quotes"]
----
fun speakerName(name: String, firstname: String) : String {
    return "${name} ${firstname}"
}
----

[NOTE.speaker]
--
* fun
* globale / locale
* déclaration paramètre
* type retour
** Unit si pas de retour
--


=== PARAMETRE FACULTATIF



[source,kotlin,subs="quotes"]
----
fun printHello(name: String?): Unit {
  if (name != null)
    println("Hello ${name}")
  else
   println("Hi there!")
}
----

=== VALEUR PAR DÉFAUT



[source,kotlin,subs="quotes"]
----
fun printMessage(name: String = "bières"): Unit {
    println("Au SfeirLunch on aime les ${name} !")
}

fun main(args: Array<String>) {
    printMessage()                  // Au SfeirLunch on aime les bières!
    printMessage("petits fours")           // Au SfeirLunch on aime les petits fours !
}
----

=== PARAMETRE NOMMÉ


[source,kotlin]
----
fun buildMessage(
        name: String,
        upper: Boolean,
        withEmoji: Boolean
): String {

 // ...

}
----


=== PARAMETRE NOMMÉ

[source,kotlin]
----
val message = buildMessage("Christopher Lloyd", true, false)

// On ne sait jamais, peut-être qu'on se rencontrera un jour futur.
----


=== PARAMETRE NOMMÉ

[source,kotlin]
----
val messageWithNamedArguments = buildMessage("Christopher Lloyd", 
  withEmoji = false, 
  upper = true)
----


=== CLASS

// Adrien

[source,kotlin,subs="quotes"]
----
class UserServices(val name: String, val age: Int) {
  var credentials: Credentials;
  fun getUsers()...
}

val userServices = UserServices('Marty', 33)
----

=== OBJECT CLASS

// Adrien

[source,kotlin,subs="quotes"]
----
object class MySingleton
----

[NOTE.speaker]
--
Pas de constructeur
--

=== DATA CLASS

// Adrien

[source,kotlin,subs="quotes"]
----
data class User(var name: String, var age: Int)
----

[NOTE.speaker]
--
pas besoin de lombock
--

=== DATA CLASS

// adrien

[source,kotlin,subs="quotes"]
----
people.copy(age = 32)
----

=== OVERLOADING JAVA

//adrien

[source,java,subs="quotes"]
----
class RegisterService{
  ...
  public RegisterService(String speaker){
    this.speaker = speaker;
    this.isFromAuvergne = true;
  }
  public RegisterService(String speaker, String organizer){
    this.speaker = speaker;
    this.organizer = organizer;
    this.isFromAuvergne = true;
  }
  public RegisterService(String speaker, String organizer, Boolean isFromAuvergne){
    this.speaker = speaker;
    this.organizer = organizer;
    this.isFromAuvergne = isFromAuvergne;
  }
}
----


=== OVERLOADING KOTLIN

//adrien

[source,kotlin,subs="quotes"]
----
class RegisterService(val speaker: String, val organizer: String?, val isFromAuvergne = true)














// Sfeir
----

== FLOW CONTROL

=== IF

[source,kotlin,subs="quotes"]
----
val marty = Speaker("Marty")
val adrien = Speaker("Adrien")

// The program runs some mystic algorithms

val speakerForSfeirLunch = if (marty.isNotInThePast()) {
  print("Choose ${marty.name}")
  marty
} else {
  print("Choose ${adrien.name}")
  adrien
}
----

[NOTE.speaker]
--
* expression => renvoit une valeur
* remplace l'opérateur ternaire
* derniere expression = valeur renvoyée
--
=== IF

[source,kotlin,subs="quotes"]
----
val marty = Speaker("Marty")
val adrien = Speaker("Adrien")

// The program runs some mystic algorithms

val speakerForSfeirLunch = if (marty.isNotInThePast()) {
  print("Choose ${marty.name}")
  if(marty.isNotInTheFutur){
    marty
  } else {
    null
  }
} else {
  print("Choose ${adrien.name}")
  adrien
}
----

=== ELVIS OPERATOR

[source,kotlin,subs="quotes"]
----
val city = user?.address?
  .city ?: throw IllegalArgumentException("Invalid User")
----

[source,kotlin,subs="quotes"]
----
findOrder()?.let { dun(it.customer) }
----

[NOTE.speaker]
--
* simplification du null check
* invocation du reste du code que si pas null
--

=== WHEN

// Adrien

[source,kotlin,subs="quotes"]
----
val currentAction = when (year) {
  1985 -> "On the parking lot with Doc and Marty"
  1955 -> "Marty in the past"
  2015, 2030 -> "Marty in the futur"
  in 2011..2019 -> "Marty in the SfeirLunch"
  else -> {
    "Qui t'appelles « banane », banane ?"
  }
}
----

[NOTE.speaker]
--
* filtrage par motif
--

=== WHEN

// Adrien

[source,kotlin,subs="quotes"]
----
when {
  isGeorgeWantToDateLorraine() -> scareHimWithADarkVadorVoice()
  !isTheCityHallClockSettedUp() -> findDoc()
  isDeloreanFullyCharged(delorean) -> travel()
}



// Sfeir
----

=== TRY CATCH



[source,kotlin,subs="quotes"]
----
val answer: Int = try {
  parseInt(input)
}
catch (e: NumberFormatException) {
  42
}
----

[NOTE.speaker]
--
* expression => renvoit valeur
* au moins un catch ou un finally
--


== KOTLIN STANDARD LIBRARY

// Adrien

[source,kotlin,subs="quotes"]
----
val speaker = "McFly"
val year = 1985
if(speaker.isNotBlank()){
  print("BTTF with ${speaker.capitalize()}")
  print(year.plus(33))
}
----

== EXTENSIONS

Le polyfill du Java dans Kotlin

[NOTE.speaker]
--
* provient de C#
* permet d'étendre un type sans modifier la classe de départ
* ne permet pas la surcharge des méthodes déclarés dans une classe ou une interface
* on doit les importer comme les fonctions de premier niveau
--


=== EXTENSIONS

[source,kotlin,subs="quotes"]
----
open class Humain {
    fun voyager(destination: String) {
        print("Voyager ${destination}")
    }
}

class Doc : Humain() {
    fun voyagerDansLeTemps(destination: String, annee: Int) {
        // ...
    }
}

val humain: Humain = Humain()
humain.voyager("chez EP")
humain.voyagerDansLeTemps("chez EP", 1985) // won't compile

val doc: Doc = Doc()
doc.voyagerDansLeTemps("chez EP", 1985)

//Back to the EP
----


=== EXTENSIONS

[source,kotlin,subs="quotes"]
----
open class Humain {
    fun voyager(destination: String) {
        print("Voyager ${destination}")
    }
}

fun Humain.voyagerDansLeTemps(destination: String, annee: Int) {



}

val humain: Humain = Humain()
humain.voyager("chez EP")
humain.voyagerDansLeTemps("chez EP", 1985)




//Back to the Breizh
----

== FUNCTIONAL PROGRAMMING

Kotlin n'est pas un langage fonctionnel

=== POUR ALLER PLUS LOIN

[%step]
* Curryfication
* Memoization
* ...
* ARROW : http://arrow-kt.io/

[NOTE.speaker]
--
* la curryfication désigne la transformation d'une fonction à plusieurs arguments en une fonction à un argument qui retourne une fonction sur le reste des arguments.
* Pas d'implémentation de Either
* Pas de Tuple mais Pair et Triple
* Surcharge d'opérateur
* arrow-kt
** Type class : Monoid, Functor...
** Data Type : Either, Try, Option...
--

== COROUTINE

&nbsp;

&nbsp;

&nbsp;

[NOTE.speaker]
--
* coroutine => thread léger collaboratif. transition vers une autre coroutine à sa demande.

* thread => scheduleur pré-emptif.horloges synchronisent les changements de contextes.
--

=== THREAD

* Deadlocks
* Tests
* Isolation des contextes
* Performance
* limite du nombre de Thread
* Complexité

=== EN JAVA

[source,java,subs="quotes"]
----

ExecutorService executor = Executors.newWorkStealingPool();
executor.invokeAll(callables).stream()
.map(dolorean -> {
    try {
      Dolorean doloreanWithTheRightSpeed = Dolorean(dolorean.getOwner(), dolorean.getCustomization(), 88)
      TimeSpaceContinium timeSpaceContinium = new TimeSpaceContinium();
      timeSpaceContinium.travel(doloreanWithTheRightSpeed)
    }
    catch (InterruptedException | ExecutionException e) { throw new RuntimeException(e); }
}).collect(Collectors.toList());

----

=== EN RxJava

[source,java,subs="quotes"]
----
getTheCarThatTravelInTheFuture()
  .map { dolorean ->
    dolorean(speed = 88)
  }
  .subscribe { user ->
    travel(doloreanWithTheRightSpeed)
  }
----

=== COROUTINE

// Adrien

[source,kotlin,subs="quotes"]
----

launch(UI){
  val dolorean = getTheCarThatTravelInTheFuture()
  val doloreanWithTheRightSpeed = dolorean.copy(speed = 88)
  travel(doloreanWithTheRightSpeed)
}

----

=== COROUTINE

// Adrien

[source,kotlin,subs="quotes"]
----

suspend fun getTheCarThatTravelInTheFuture() {...}

launch(UI){
  val dolorean = getTheCarThatTravelInTheFuture()
  val doloreanWithTheRightSpeed = dolorean.copy(speed = 88)
  travel(doloreanWithTheRightSpeed)
}

----



[NOTE.speaker]
--
* Thead block
* suspend no blocking and return value
--

=== COROUTINE

* cancellable
* blocking
* asynchronious
* async return result
* Coroutine Exception


== KOTLIN ET LES FRAMEWORKS

=== GRADLE DSL

=== KTOR

=== SPRING BOOT

* open
* https://start.spring.io
* Exemple

=== SPRING FU

```
val app = application {
  import(beans)
  listener<ApplicationReadyEvent> {
    ref<UserRepository>().init()
  }
  properties<SampleProperties>("sample")
  server {
    port = if (profiles.contains("test")) 8181 else 8080
    codecs {
      string()
      jackson {
        indentOutput = true
      }
    }
    import(::routes)
  }
  mongodb {
    embedded()
  }
}

val beans = beans {
  bean<UserRepository>()
  bean<UserHandler>()
}

fun routes(userHandler: UserHandler) = router {
  GET("/", userHandler::listView)
  GET("/api/user", userHandler::listApi)
  GET("/conf", userHandler::conf)
}

fun main() = app.run()
```

=== OTHERS 

// Adrien

* jackson-kotlin
* Micronaut
* Javalin
* Kotlin arrow
* https://github.com/KotlinBy/awesome-kotlin

== KOTLIN MULTIPLATEFORME

* JVM
* JS
* Natif (macos, ios, windows, linux, wasm)

https://charts-kt.io/

[NOTE.speaker]
--
* Kotlin.X
--

== PROD

* American express
* Pinterest
* Expedia
* N26
* Shazam
* Saagie (Normandie)
* Oui.sncf

== TOOLS

* IntelliJ
** Convert from Java to Kotlin
* Eclipse
* VSCode (Language Server Protocol)

== THANKS

Any Question ?

https://play.kotlinlang.org/koans/Introduction/Hello,%20world!/Task.kt
https://kotlinlang.org/docs/reference/classes.html
